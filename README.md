### @nimbbl/ui

- Fully customizable React UI Components

### Usage

```tsx
import React from 'react';
import Button from '@nimbbl/ui';
import logo from './logo.svg';

const IconComponent = <img src={logo} alt="icon" />;

function App() {
  return (
    <div>
      <Button
        text="Hello"
        style={{ marginLeft: '1rem' }}
        attributes={{ onClick: () => {} }}
      />
      <Button text="Friend" style={{ margin: '1rem' }} />
      <Button text="Hello" IconComponent={IconComponent} iconPosition="right" />
    </div>
  );
}

export default App;
```

### Run project locally

```sh
# clone the repo
npm i # to install dependencies

# run react app
npm start

# run storybook
npm run storybook
```
