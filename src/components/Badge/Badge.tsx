/* eslint-disable react/require-default-props */
import React from 'react';
import './Badge.css';

export type BadgeType = 'New' | 'Completed' | 'Pending' | 'Failed';

export interface BadgeProps {
  type?: BadgeType;
}

const getBadgeClasses = (type: BadgeType): string => {
  switch (type) {
    case 'New':
      return 'new';
    case 'Completed':
      return 'completed';
    case 'Failed':
      return 'failed';
    case 'Pending':
      return 'pending';
    default:
      return '';
  }
};

export function Badge({ type = 'New' }: BadgeProps) {
  return <div className={`badge ${getBadgeClasses(type)}`}>{type}</div>;
}
