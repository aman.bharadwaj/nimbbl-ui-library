import React from 'react';
import './Modal.css';

export interface ModalProps {
  jsx: JSX.Element;
  show: boolean;
}

export function Modal({ jsx, show }: ModalProps) {
  return show ? (
    <div className="container flex justify-center mx-auto">
      <div className="absolute inset-0 flex items-center justify-center modal-overlay">
        <div className="modal">{jsx}</div>
      </div>
    </div>
  ) : null;
}
