import React, { LiHTMLAttributes } from 'react';
import { RiMore2Fill } from 'react-icons/ri';
import './ExpandMore.css';

export interface ListItem {
  key: string | number;
  text: string;
  attributes?: LiHTMLAttributes<HTMLLIElement>;
}

export interface ExpandMoreProps {
  items: ListItem[];
}

export function ExpandMore({ items }: ExpandMoreProps) {
  return (
    <div className="dropdown inline-block relative">
      <RiMore2Fill className="more-icon" />
      <ul className="dropdown-menu absolute hidden">
        {items.map((el) => (
          <li
            key={el.key}
            className="dropdown-menu-item"
            {...(el.attributes ?? {})}
          >
            <span>{el.text}</span>
          </li>
        ))}
      </ul>
    </div>
  );
}
