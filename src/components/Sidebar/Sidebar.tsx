import React from 'react';
import { IconType } from 'react-icons';
import './Sidebar.css';

export type NavItem = {
  id: string | number;
  Icon: IconType;
  text: string;
  children?: NavItem[];
  secondary?: boolean;
};

export interface SidebarProps {
  navItems: NavItem[];
  logoUrl: string;
  profilePhotoUrl: string;
}

export function Sidebar({ navItems, logoUrl, profilePhotoUrl }: SidebarProps) {
  return (
    <div className="sidebar">
      <div className="logo">
        <img src={logoUrl} alt="logo" />
      </div>
      <ul className="nav-item-list">
        {navItems.map(
          ({ id, Icon, text, secondary }) =>
            !secondary && (
              <li key={id} className="nav-item">
                <Icon className="nav-item-icon" />
                {text}
              </li>
            )
        )}
        <div className="divider" />
        {navItems.map(
          ({ id, Icon, text, secondary }) =>
            secondary && (
              <li key={id} className="nav-item">
                <Icon className="nav-item-icon" />
                {text}
              </li>
            )
        )}
      </ul>

      <div className="profile">
        <img src={profilePhotoUrl} alt="profile" />
        <span>Profile</span>
      </div>
    </div>
  );
}
