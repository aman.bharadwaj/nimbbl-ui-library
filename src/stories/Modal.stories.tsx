/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Modal, InputField, Button, SelectList } from '../components';

export default {
  title: 'Modal',
  component: Modal,
} as ComponentMeta<typeof Modal>;

const Template: ComponentStory<typeof Modal> = function ModalTemplate(args) {
  return <Modal {...args} />;
};

const MyJSX = (
  <div>
    <div style={{ marginBottom: '1rem' }}>
      <InputField id="name" label="Name" />
    </div>
    <div style={{ margin: '1rem 0 1rem 0' }}>
      <InputField id="email" label="Email" />
    </div>
    <div style={{ margin: '1rem 0 1rem 0' }}>
      <SelectList
        id="task"
        options={[
          { key: 1, displayName: 'Hello', value: 'hello' },
          { key: 2, displayName: 'Hello', value: 'hello' },
          { key: 3, displayName: 'Hello', value: 'hello' },
        ]}
      />
    </div>
    <Button text="Hello" style={{ marginRight: '0.2rem' }} />
  </div>
);

export const Default = Template.bind({});
Default.args = { jsx: MyJSX, show: true };
