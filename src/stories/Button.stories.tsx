/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { RiArrowDownLine } from 'react-icons/ri';
import { Button } from '../components';

export default {
  title: 'Button',
  component: Button,
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = function ButtonTemplate(args) {
  return <Button {...args} />;
};

export const Default = Template.bind({});
Default.args = {
  text: 'Button',
  attributes: {
    onClick: () => {},
  },
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  text: 'Button',
  IconComponent: RiArrowDownLine,
  iconPosition: 'right',
  attributes: {
    onClick: () => {},
  },
};

export const IconOnly = Template.bind({});
IconOnly.args = {
  IconComponent: RiArrowDownLine,
  iconPosition: 'center',
};
