/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import {
  RiPieChartLine,
  RiStarLine,
  RiSettingsLine,
  RiLogoutCircleLine,
} from 'react-icons/ri';
import { NavItem, Sidebar } from '../components';

export default {
  title: 'Sidebar',
  component: Sidebar,
} as ComponentMeta<typeof Sidebar>;

const Template: ComponentStory<typeof Sidebar> = function SidebarTemplate(
  args
) {
  return <Sidebar {...args} />;
};

const navItems: NavItem[] = [
  {
    id: 1,
    text: 'Analytics',
    Icon: RiPieChartLine,
  },
  {
    id: 2,
    text: 'Issuers',
    Icon: RiStarLine,
  },
  {
    id: 3,
    text: 'Roles & Access',
    Icon: RiSettingsLine,
  },
  {
    id: 4,
    text: 'Settings',
    Icon: RiSettingsLine,
    secondary: true,
  },
  {
    id: 5,
    text: 'Log out',
    Icon: RiLogoutCircleLine,
    secondary: true,
  },
];

export const Default = Template.bind({});
Default.args = {
  navItems,
  profilePhotoUrl:
    'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
  logoUrl:
    'https://images.unsplash.com/photo-1633356122544-f134324a6cee?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80',
};
