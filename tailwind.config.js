module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        white: {
          white: '#ffffff',
          bg1: '#F1F4F',
          bg2: '#F6F8FC',
          bg3: '#FAFAFC',
        },
        black: {
          primary: '#0F1A2A',
          secondary: '#475569',
          tertiary: '#94A3B8',
          dividers: '#CBD4E1',
        },
        red: {
          10: '#FFEBEB',
          20: '#FC9595',
          40: '#D83232',
          60: '#B01212',
          80: '#8C0000',
        },
        green: {
          10: '#E8FCF1',
          20: '#A5E1BF',
          40: '#219653',
          60: '#00632B',
          80: '#00401C',
        },
        yellow: {
          10: '#FFF5D5',
          20: '#FFDE81',
          40: '#FFCC00',
          60: '#E5B800',
          80: '#926B04',
        },
        info: {
          10: '#D3E1FE',
          20: '#7EA5F8',
          40: '#4D82F3',
          60: '#0067DB',
          80: '#0037B3',
        },
        purple: {
          10: '#F5E8FF',
          20: '#D8AAFD',
          40: '#AC54F0',
          60: '#8219D5',
          80: '#530094',
        },
        orange: {
          10: '#FFF8E5',
          20: '#FCCC75',
          40: '#FDAC42',
          60: '#FF8800',
          80: '#E57A00',
        },
        'primary-border': '#131DF5',
        secondary: '#F2F2F2',
        'secondary-text': '#333333',
      },
    },
  },
  plugins: [],
};
